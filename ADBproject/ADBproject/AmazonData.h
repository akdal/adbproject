#pragma once

#include"Item.h"
#include"Review.h"
#include"Category.h"
#include<memory>

class AmazonData{
public:
	int itemcnt;
	std::shared_ptr<Item> *items;
	
	AmazonData(int itemcnt){
		this->itemcnt = itemcnt;
		this->items = new std::shared_ptr<Item>[itemcnt];
	}

	int getItemCount() const
	{ return this->itemcnt; }

	void registerItem(std::shared_ptr<Item> itm){
		int id = itm->itemId;
		_ASSERTE(this->items[id] == NULL);
		this->items[id] = itm;
	}
	std::shared_ptr<Item> getItem(int id) const
	{ return items[id]; }
};