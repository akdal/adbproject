#ifndef _TIMEGRAPH_H
#define _TIMEGRAPH_H

#include<vector>
#include<memory>
#include<map>
#include<queue>
#include"GraphNode.h"

typedef std::pair <double, std::pair<int, int> > tpi;

class TimeGraph{
public:
	int timecolumncnt;
	int numofgroup;
	std::vector <int> group;
	std::vector <int> groupCntForTimecolumn;
	std::vector<std::shared_ptr<GraphNode> > nodes;
	std::map<std::shared_ptr<GraphNode>, std::shared_ptr<GraphNode> > adjlist;
	std::vector<std::shared_ptr<GraphNode> > *nodesByTimecolumn;
	std::map<int, int> timecolumnidx;

	tpi make_tpair(double a, int b, int c){
		tpi temp;
		temp.first = a;
		temp.second.first = b;
		temp.second.second = c;

		return temp;
	}
	TimeGraph(RawTimeGraph *rtg){
		this->timecolumncnt = rtg->timecolumns.size();
		this->nodesByTimecolumn = new std::vector<std::shared_ptr<GraphNode> >[this->timecolumncnt];
		this->group.resize(this->nodes.size());
		this->groupCntForTimecolumn.resize(this->timecolumncnt);
		numofgroup = this->nodes.size();
		this->nodes = rtg->nodes;
//		this->adjlist =rtg->adjlist;
		this->adjlist.clear();
		for(int i=0;i<nodes.size();i++){
			this->adjlist[nodes[i]] = nodes[rtg->adjlist[i]];
		}
		int i = 0;
		for(auto itr = rtg->timecolumns.begin(); itr != rtg->timecolumns.end(); itr++){
			timecolumnidx[*itr] = i;
			i++;
		}
		for(auto itr = this->nodes.begin(); itr != this->nodes.end(); itr++){
			std::shared_ptr<GraphNode> p = (*itr);
			nodesByTimecolumn[timecolumnidx[p->timecolumn]].push_back(p);
		}
	}

	int Find(int x){
		if(x == group[x]) return x;
		else{
			group[x] = Find(group[x]);
			return group[x];
		}
	}
	bool Union(int a, int b){
		a = Find(a);
		b = Find(b);
		if(a == b) return false;
		else{
			group[b] = a;
			return true;
		}
	}
	void Contract(double thres = 0.5){
		for(int i=0;i<nodes.size();i++) group[i] = i;
		for(int i=0;i<this->timecolumncnt;i++){
			printf(" Contract start for time column %d ... \n", i);
			groupCntForTimecolumn[i] = nodesByTimecolumn[i].size();
			std::priority_queue <tpi, std::vector <tpi>, std::less<tpi> > Queue;

			while(!Queue.empty()) Queue.pop();

			for(int j=0;j<nodesByTimecolumn[i].size();j++){
				for(int k=j+1;k<nodesByTimecolumn[i].size();j++){
					Queue.push(make_tpair(CategoryList::similarity(nodesByTimecolumn[i][j]->categories, nodesByTimecolumn[i][k]->categories), j, k));
				}
			}
			while(!Queue.empty()){
				tpi top = Queue.top();
				Queue.pop();

				if(top.first >= thres){
					if(Union(top.second.first, top.second.second)){
						groupCntForTimecolumn[i]--;
					}
				}
				else break;
			}
			printf(" Contract is completed for time column %d ! \n", i);
		}
		for(int i=0;i<nodes.size();i++) group[i] = Find(group[i]);
	}
	int GetNumberOfCommonTrajectory(int from, int to, std::shared_ptr <GraphNode> x, std::shared_ptr <GraphNode> y, double similarity_thres, int thres){
		int ret_value = 1;
		for(int i=from+1;i<=to;i++){
			x = adjlist[x];
			y = adjlist[y];

			double similarity = CategoryList::similarity(x->categories, y->categories);

			if(similarity >= similarity_thres) ret_value++;
			//else if(similarity <= limit) return -1;

			if(ret_value >= thres) break;
		}
		return ret_value;
	}
	int GetNumberOfCommonInterest(int from, int to, double similarity_thres = 0.5, int thres = -1){
		double factor = 0.5;
		int same_group = 0;
		int ret_value = 0;
		int count = 0;
		int really = 0;
		int group_no = nodesByTimecolumn[from].size();
		if(thres == -1) thres = (to - from) * factor;

		group.resize(nodesByTimecolumn[from].size());
		for(int i=0;i<nodesByTimecolumn[from].size();i++) group[i] = i;
		for(int i=0;i<nodesByTimecolumn[from].size();i++){
			for(int j=i+1;j<nodesByTimecolumn[from].size();j++){
				//printf("Consider node %d and %d on Time column %d.... ",i, j, from);

				std::shared_ptr <GraphNode> x = nodesByTimecolumn[from][i];
				std::shared_ptr <GraphNode> y = nodesByTimecolumn[from][j];

				double similarity = CategoryList::similarity(x->categories, y->categories);

				//printf("similarity is %.2lf\n",similarity);
				if(similarity >= similarity_thres){
					same_group++;
					if(GetNumberOfCommonTrajectory(from, to, x, y, similarity_thres, thres) >= thres){
						ret_value++;
						if(GetNumberOfCommonTrajectory(from, 110, x, y, similarity_thres, (110 - from) * factor) >= (110 - from) * factor){
							really++;
						}
						if(Union(i, j)){
							group_no--;
							count++;
						}
					}
				}

				//printf("done!, current ret_value is %d, group # is %d, same group # is %d\n", ret_value, group_no, same_group);
			}
			if(i % 400 == 0){
				printf("%d : ret_value : %d, group # : %d(%d), same group # : %d, real : %d\n", i, ret_value, group_no, count, same_group, really);
			}
		}

		printf("end : ret_value : %d, group # : %d(%d), same group # : %d, real : %d\n", ret_value, group_no, count, same_group, really);

		return ret_value;
	}
	int GetNumOfGroups(int x) { return groupCntForTimecolumn[x]; }
};

#endif