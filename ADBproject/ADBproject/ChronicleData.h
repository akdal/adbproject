#ifndef _CHRONICLEDATA_H
#define _CHRONICLEDATA_H

#include<algorithm>
#include<vector>
#include<memory>
#include"Review.h"

class ChronicleData{
public:
	int usercnt;
	std::vector<std::shared_ptr<Review> > *reviewsByTime;
	
	ChronicleData(int usrcnt){
		_ASSERTE(usrcnt >= 0);
		this->usercnt = usrcnt;
		this->reviewsByTime = new std::vector<std::shared_ptr<Review> >[usrcnt];
	}
	void addReview(int usrid, std::shared_ptr<Review> r)
	{ reviewsByTime[usrid].push_back(r); }

	void sortReviews();
	/**
	returns : added element number of usrs
	*/
	int getUserList(std::vector<int> *usrs, int minreviewcnt, int maxreviewcnt){
		int sz = 0;
		for(int i = 0; i < this->usercnt; i++){
			if(reviewsByTime[i].size() >= minreviewcnt && reviewsByTime[i].size() <= maxreviewcnt){
				if(usrs)
					usrs->push_back(i);
				sz++;
			}
		}
		return sz;
	}
};

#endif