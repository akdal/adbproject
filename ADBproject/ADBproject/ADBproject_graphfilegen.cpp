#include"amazonDataGenerator.h"
#include<stdio.h>
#include"NodeGen_Type1.h"
#include"NodeGen_Type2.h"
#include"NodeGen_Type3.h"
#include"graphfileIO.h"
#include<algorithm>
using namespace std;

int main(int argc, char **argv)
{
	if(argc< 3){
		printf("ADBproject_graphfilegen.exe <amazon-meta.txt> <graphfile.txt>\n");
		return 0;
	}

	FILE *f = fopen(argv[1], "r");
	FILE *gf = fopen(argv[2], "w");
	if(!f){
		printf("%s file doesn't exist\n", argv[1]);
		return 0;
	}

	printf("ADBproject graph generator start!\n");
	AmazonData *amazonData = readFile(f, true, true);
	printf("Now Making chronicle..\n");
	ChronicleData *chronicleData = generateChronicleData(amazonData);
	printf("Done.\n");
	
	vector<int> usrlistCnt;
	printf("Usr cnt over(or eq to) review number of 5 : %d\n", chronicleData->getUserList(NULL, 5, 10000));
	printf("Usr cnt over(or eq to) review number of 10 : %d\n", chronicleData->getUserList(NULL, 10, 10000));
	printf("Usr cnt over(or eq to) review number of 20 : %d\n", chronicleData->getUserList(&usrlistCnt, 20, 10000));
	printf("Usr cnt over(or eq to) review number of 100 : %d\n", chronicleData->getUserList(NULL, 100, 10000));

	printf("Now making timeline graph.. userlist cnt : %d\n", usrlistCnt.size());
	printf("Erasing non-related reviews..\n");
	sort(usrlistCnt.begin(), usrlistCnt.end());
	
	for(int i = 0; i < amazonData->itemcnt; i++){
		if(amazonData->items[i]){
			amazonData->items[i]->reviews.clear();
		}
	}

	printf("reviews clearing end!\n");

	NodeGenerator *nodeGenerator = new NodeGen_Type3(amazonData, 3);//new NodeGen_Type1(amazonData, 3);
	RawTimeGraph *tg = generateRawTimeGraph(chronicleData, usrlistCnt, nodeGenerator);
	printf("Generated : \n");
	printf("\tnode cnt : %d\n", tg->totalNodeCount());
	printf("\tedge cnt : %d\n", tg->totalEdgeCount());

	writeGraph(gf, tg);

	printf("Writing end.\n");

	return 0;
}