/*
Reads preprocessed file(amazon-meta-preprocessed.txt), and make AmazonData class.
*/

#include<stdio.h>
#include<memory>
#include"amazonDataGenerator.h"
#include"NodeGen_Type1.h"
using namespace std;

int main(int argc, char **argv)
{
	if(argc< 2){
		printf("ADBproject_readtest.exe <amazon-meta.txt>\n");
		return 0;
	}

	FILE *f = fopen(argv[1], "r");
	if(!f){
		printf("%s file doesn't exist\n", argv[1]);
		return 0;
	}

	printf("ADBprojectFileReader start!\n");
	AmazonData *amazonData = readFile(f, true, true, 5000);
	printf("Now Making chronicle..\n");
	ChronicleData *chronicleData = generateChronicleData(amazonData);
	printf("Done.\n");
	
	vector<int> usrlistCnt;
	printf("Usr cnt over(or eq to) review number of 5 : %d\n", chronicleData->getUserList(NULL, 5));
	printf("Usr cnt over(or eq to) review number of 10 : %d\n", chronicleData->getUserList(&usrlistCnt, 10));
	printf("Usr cnt over(or eq to) review number of 20 : %d\n", chronicleData->getUserList(NULL, 20));
	printf("Usr cnt over(or eq to) review number of 100 : %d\n", chronicleData->getUserList(NULL, 100));

	printf("Now making timeline graph.. userlist cnt : %d\n", usrlistCnt.size());
	NodeGen_Type1 *nodeGenerator = new NodeGen_Type1(amazonData, 7);
	TimeGraph *tg = generateTimeGraph(chronicleData, usrlistCnt, nodeGenerator);
	printf("Generated time columns cnt : %d\n", tg->totalTimeColumnCnt());
	printf("\tnode cnt : %d\n", tg->totalNodeCount());
	printf("\tedge cnt : %d\n", tg->totalEdgeCount());

	while(true){
		printf("1. User's chronicle / 2. Item's info\n");
		int choose;
		scanf("%d", &choose);
		if(choose == 1){
			int usrid;
			printf("Input user id(max usr id : %d) : ", chronicleData->usercnt - 1);
			scanf("%d", &usrid);

			printf("user %d's review size : %d\n", usrid, chronicleData->reviewsByTime[usrid].size());
			for(int i = 0; i < chronicleData->reviewsByTime[usrid].size(); i++){
				printf("\t");
				chronicleData->reviewsByTime[usrid][i]->print();
				printf("\n");
			}
		}else if(choose == 2){
			int itmid;
			printf("Input item id : ");
			scanf("%d", &itmid);

			shared_ptr<Item> itm = amazonData->getItem(itmid);
		
			printf("-------\n");
			if(itm== NULL)
				printf("id %d : Null item\n", itmid);
			else{
				printf("id %d : \n", itmid);
				printf("ASNid : %s, title : [%s]\n", itm->ASINid.c_str(), itm->title.c_str());
				printf("group : [%s]\n", itm->group.c_str());
				printf("similarItemes : \n");
				for(int i = 0; i < itm->similarItemASINs.size(); i++)
					printf("\sid :%s\n", itm->similarItemASINs[i].c_str());
				printf("categories : \n");
				for(int i = 0; i < itm->categories->size(); i++){
					printf("\t");
					(*itm->categories)[i]->print();
					printf("\n");
				}
				printf("reviews : \n");
				for(int i = 0; i < itm->reviews.size(); i++){
					printf("\t");
					itm->reviews[i]->print();
					printf("\n");
				}
			}
			printf("-------\n");
		}else{
			printf("Wrong input. type 1 or 2.\n");
		}
	}
	
	return 0;
}