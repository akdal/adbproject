#ifndef _GRAPHNODE_H
#define _GRAPHNODE_H

#include<vector>
#include"CategoryList.h"
class RawTimeGraph;

class GraphNode{
public: GraphNode(int nodeid){
		this->nodeid = nodeid;
	}
public:
	int timecolumn;
	int nodeid;
	CategoryList categories;
	std::vector<int> userids;

	friend class RawTimeGraph;

	void addUserID(int userid){
		this->userids.push_back(userid);
	}

	bool operator<(const GraphNode &cn){
		return this->nodeid < cn.nodeid;
	}
	bool operator==(const GraphNode &cn){
		return this->nodeid == cn.nodeid;
	}
};


#include"RawTimeGraph.h"
#endif