#ifndef _REVIEW_H
#define _REVIEW_H


#include<vector>

class Review{
public:
	int dates;
	int customer;
	int rating;
	int vote;
	int helpfulness;
	int itemId;

	void print(){
		printf("date:%d, customer:%d, rating:%d, vote:%d, helpfulness:%d, itemId:%d\n", dates, customer, rating, vote, helpfulness,
			itemId);
	}
};

#endif