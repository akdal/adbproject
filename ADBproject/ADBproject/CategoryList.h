#pragma once

#include<vector>
#include<memory>
#include"Category.h"
class CategoryList{
public:
	static void merge(CategoryList *a, CategoryList *dst);
	static double similarity(const CategoryList &a, const CategoryList &b);

	std::vector<std::shared_ptr<Category> > v;

	int size() const
	{ return v.size(); }
	std::shared_ptr<Category> &operator[](int idx)
	{ return v[idx]; }
	bool operator<(const CategoryList &cl) const
	{ return this->v < cl.v; }
	bool operator==(const CategoryList &cl) const
	{ return this->v == cl.v; }
	void push_back(std::shared_ptr<Category> &sp)
	{ v.push_back(sp); }

	void sort();
};
