#pragma once

#include"AmazonData.h"
#include"ChronicleData.h"
#include"NodeGenerator.h"
#include"RawTimeGraph.h"
AmazonData *readFile(FILE *f, bool readsSimilarItem, bool readsCategory, int maxItemCnt=-1);
ChronicleData *generateChronicleData(AmazonData *ad);
RawTimeGraph *generateRawTimeGraph(ChronicleData *cdata, std::vector<int> &targetUserIds, NodeGenerator *ng);
