#include<memory>
#include<stdio.h>
#include<string>
using namespace std;

void printInf(shared_ptr<int> &a, shared_ptr<int> &b){
	printf("a value : %d, b value : %d\n", *a, *b);
	printf("\ta == b : %d\n", a == b);
	printf("\t*a == *b : %d\n", *a == *b);
	printf("\ta < b : %d\n", a < b);
	printf("\ta > b : %d\n", a > b);
}

int main(){
	shared_ptr<int> nullp(NULL);
	printf("null test : %d\n", nullp == NULL);
	shared_ptr<int> pa(new int(1));
	shared_ptr<int> pb(new int(2));
	
	printInf(pa, pb);
	printInf(pb, pa);
	pb = shared_ptr<int>(new int(1));
	printInf(pa, pb);

	return 0;
}
