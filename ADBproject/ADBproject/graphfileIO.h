#ifndef _GRAPHFILEIO_H
#define _GRAPHFILEIO_H

#include<stdio.h>
#include"RawTimeGraph.h"
void writeGraph(FILE *f, const RawTimeGraph *tg);
RawTimeGraph *readGraph(FILE *f);

#endif