#include"RawTimeGraph.h"
using namespace std;

std::shared_ptr<GraphNode> RawTimeGraph::addNode(){
	totNodeCnt++;
	auto t = std::shared_ptr<GraphNode>(new GraphNode(nodes.size()));
	nodes.push_back(t);
	return t;
}
std::shared_ptr<GraphNode> RawTimeGraph::addNode(int id){
	totNodeCnt++;
	auto t = std::shared_ptr<GraphNode>(new GraphNode(id));
	if(!(id == nodes.size()))
		printf("STRANGE!! %d vs. %d\n", id, nodes.size());
	nodes.push_back(t);
	return t;
}

void RawTimeGraph::addEdge(std::shared_ptr<GraphNode> from, std::shared_ptr<GraphNode> to){
	_ASSERTE(from->timecolumn < to->timecolumn);
	
	this->timecolumns.insert(from->timecolumn);
	this->timecolumns.insert(to->timecolumn);
	adjlist[from->nodeid] = to->nodeid;
}