#include"graphfileIO.h"
#include"RawTimeGraph.h"
#include"TimeGraph.h"
#include"CategoryList.h"
#include<stdio.h>
using namespace std;

/*
../../../amazon-meta.txt/graphfile_3days_over20_type1.txt
*/
void testCategoryList(){
	CategoryList al;
	{
		al.v.push_back(Category::parseCategory("|a[0]|b[1]|c[2]"));
		al.v.push_back(Category::parseCategory("|a[0]|b[1]|d[3]"));
		al.v.push_back(Category::parseCategory("|e[4]|f[5]"));
	}
	CategoryList bl;
	{
		bl.v.push_back(Category::parseCategory("|a[10]|b[1]|c[2]"));
		bl.v.push_back(Category::parseCategory("|a[0]|b[1]|d[4]"));
		bl.v.push_back(Category::parseCategory("|e[14]|f[5]"));
		bl.v.push_back(Category::parseCategory("|g[16]"));
	}
	double dd = CategoryList::similarity(al, bl);
	printf("dd %lf\n", dd);
}
int main(){
	//testCategoryList();

	char graphfileName[100];
	printf("Type graph file name : ");
	scanf("%s", graphfileName);

	FILE *f = fopen(graphfileName, "r");
	if(f == NULL){
		printf("No such file!!\n");
		return 0;
	}
	RawTimeGraph *rtg = readGraph(f);
	fclose(f);

	printf("Reading end!! total nodes : %d, total edges : %d, total timecolumns :%d\n", rtg->totalNodeCount(), rtg->totalEdgeCount(), rtg->timecolumns.size());
	int ttt = 1;
	for(auto itr = rtg->timecolumns.begin(); itr != rtg->timecolumns.end(); itr++)
		printf("\t%d: %d\n", ttt, *itr), ttt++;
	TimeGraph *tg = new TimeGraph(rtg);
	printf("Node count per timecolumsn : \n");
	for(int i = 0; i < tg->timecolumncnt; i++)
		//printf("\ttimecolumn %d : %d nodes\n", i, tg->nodesByTimecolumn[i].size());
		printf("\t%d\n", tg->nodesByTimecolumn[i].size());
	
	while(true);
	int start = 40;
	//int end = 60;
	for(int i=50;i<=90;i+=10){
		printf("start calculation from %d to %d ... \n",start, i);
		printf("from %d to %d : %d ! \n",start, i, tg->GetNumberOfCommonInterest(start, i));
		printf("calculation end! \n");
	}
	/*
	printf("Clustering.....\n");
	//tg->Contract();

	printf("Contraction end!!\n");
	strcat(graphfileName, ".groupnames");
	
	
	printf("Now writing to %s...\n", graphfileName);
	FILE * writef =fopen(graphfileName, "w");
	fprintf(writef, "timecolumnsize %d\n", tg->timecolumncnt);
	for(int i = 0; i < tg->timecolumncnt; i++)
		fprintf(writef, "\t%d\n", tg->GetNumOfGroups(i));
	for(int i = 0; i < tg->group.size(); i++)
		fprintf(writef, "%d ", tg->group[i]);
	
	fclose(writef);
	printf("END!!\n");
	
	*/
	scanf("%*s");
	return 0;
}