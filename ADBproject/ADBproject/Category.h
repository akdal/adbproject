#ifndef _CATEGORY_H
#define _CATEGORY_H

/*
item의 카테고리.
*/
#include<vector>
#include<algorithm>
#include<crtdbg.h>
#include<memory>

class Category{
public:
	static std::shared_ptr<Category> parseCategory(const char *msg){
		int p = 0;
		std::shared_ptr<Category> cate(new Category());
		while(msg[p]){
			_ASSERTE(msg[p] == '|');
			p++;
			int ii = 0;
			bool start = false;
			while(msg[p] != '|' && msg[p] != 0){
				if(msg[p] == '['){
					start = true;
				}else if(msg[p] == ']')
					start= false;
				else if(start){
					ii = ii * 10 + msg[p] - '0';
				}
				p++;
			}
			cate->cdata.push_back(ii);
		}
		return cate;
	}
	static double jaccardSimilarity(const Category *c1, const Category *c2){
		int c1size = c1->cdata.size();
		int c2size = c2->cdata.size();
		int denom = std::max(c1size, c2size);
		int num = 0;

		for(int i = 0; i< c1size && i < c2size; i++){
			if(c1->cdata[i] == c2->cdata[i])
				num++;
			else
				break;
		}
		return num / (double)denom;
	}
	static double jaccardSimilarity(const Category &c1, const Category &c2){
		int c1size = c1.cdata.size();
		int c2size = c2.cdata.size();
		int denom = std::max(c1size, c2size);
		int num = 0;

		for(int i = 0; i< c1size && i < c2size; i++){
			if(c1.cdata[i] == c2.cdata[i])
				num++;
			else
				break;
		}
		return num / (double)denom;
	}
	std::vector<int> cdata;

	bool equals(const Category &cc){
		return this->cdata == cc.cdata;
	}
	bool operator<(const Category &c)
	{
		return this->cdata < c.cdata;
	}
	bool operator==(const Category &c)
	{
		return this->cdata == c.cdata;
	}
	void print(){
		for(int i = 0; i < cdata.size(); i++)
			printf("|%d", cdata[i]);
	}
};

#endif