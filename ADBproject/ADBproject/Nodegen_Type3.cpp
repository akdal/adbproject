#include"NodeGen_Type3.h"
using namespace std;

static shared_ptr<CategoryList> _getCategoryOf(const AmazonData *ad, shared_ptr<Review> rv){
	return ad->getItem(rv->itemId)->categories;
}

void NodeGen_Type3::genNext(shared_ptr<GraphNode> &gn){
	_ASSERTE(hasNext());

	shared_ptr<Review> rv = this->reviews->at(ptr);

	int startdate = rv->dates;
	gn->addUserID(this->userid);
	gn->categories.v = (_getCategoryOf(this->amazonData, rv))->v;
	gn->timecolumn = this->totaltimecolcnt;
	ptr++;
	int sz = this->reviews->size();
	while(ptr < sz){
		rv = this->reviews->at(ptr);
		
		int nexttimecolumn = (rv->dates - startdate);

		if(nexttimecolumn < this->dateRange){
			CategoryList::merge(&(*(_getCategoryOf(this->amazonData, rv))), &gn->categories);
			ptr++;
		}else{
			break;
		}
	}
	this->totaltimecolcnt++;
}