#ifndef _NODEGENERATOR_H
#define _NODEGENERATOR_H

#include"GraphNode.h"
#include"Review.h"
#include<vector>
#include<memory>

class NodeGenerator{
public:
	virtual void init(const std::vector<std::shared_ptr<Review> > &usr_reviews, int userid) = 0;
	virtual bool hasNext() = 0;
	virtual void genNext(std::shared_ptr<GraphNode> &p) = 0;
};

#endif