#include"TimeGraph.h"

	std::priority_queue <tpi, std::vector <tpi>, std::less<tpi> > Queue;

	void TimeGraph::Contract(double thres){
		for(int i=0;i<nodessize;i++) group[i] = i;
		for(int i=0;i<this->timecolumncnt;i++){
			printf(" Contract start for time column %d ... \n", i);

			groupCntForTimecolumn[i] = nodesByTimecolumn[i].size();

			while(!Queue.empty()) Queue.pop();

			int originalcnt = groupCntForTimecolumn[i];
			printf("start! \n");
			for(int j=0;j<nodesByTimecolumn[i].size();j++){
				for(int k=j+1;k<nodesByTimecolumn[i].size();k++){
					Queue.push(make_tpair(CategoryList::similarity(nodesByTimecolumn[i][j]->categories, nodesByTimecolumn[i][k]->categories), j, k));
				}
			}
			printf("set queue ended\n");
			while(!Queue.empty()){
				tpi top = Queue.top();
				Queue.pop();

				printf("\ttop.first : %lf\n", top.first);
				if(top.first >= thres){
					if(Union(top.second.first, top.second.second)){
						groupCntForTimecolumn[i]--;
					}
				}
				else break;
			}
			printf(" Contract is completed for time column %d ! --> %d->%d\n", i, originalcnt, groupCntForTimecolumn[i]);

			std::vector<std::shared_ptr<GraphNode> >().swap(nodesByTimecolumn[i]);
		}
		for(int i=0;i<nodessize;i++) group[i] = Find(group[i]);
	}