#ifndef _RAWTIMEGRAPH_H
#define _RAWTIMEGRAPH_H

#include<map>
#include<vector>
#include<set>
#include<CrtDbg.h>

#include"GraphNode.h"
class RawTimeGraph{
public:
	std::vector<std::shared_ptr<GraphNode> > nodes;
	std::map<int, int> adjlist;
	int totNodeCnt;
	std::set<int> timecolumns;

	RawTimeGraph(){
		totNodeCnt = 0;
	}

	std::shared_ptr<GraphNode> addNode();
	std::shared_ptr<GraphNode> addNode(int id);
	void addEdge(std::shared_ptr<GraphNode> from, std::shared_ptr<GraphNode> to);

	int totalNodeCount() const
	{ return this->totNodeCnt; }
	int totalEdgeCount() const
	{ return this->adjlist.size(); }
};

#endif