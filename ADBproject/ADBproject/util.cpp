#include"util.h"
#include<stdio.h>
#include<memory.h>

static int __DATE_DATES[12] = {
31, 28, 31, 30, 31, 30, 31,
31, 30, 31, 30, 31
};
static int __DATE_DATES_SUM[12] = {
0, 31, 59, 90, 120, 151, 181, 212,
243, 273, 304, 334
};


int date_strToInt(const char *ccd){
	char cd[1000];
	int l =strlen(ccd);
	memcpy(cd, ccd, l + 1);

	int y, m, d;

	for(int i =0; i < l; i++)
		cd[i] = cd[i] == '-' ? ' ' : cd[i];
	sscanf(cd, "%d %d %d", &y, &m, &d);
	
	int dd = 0;
	int yunnyeoncnt = ((y - 1) / 4) - ((y - 1) / 100) + ((y - 1) / 400);
	int ydate = ((y - 1) - yunnyeoncnt) * 365 + yunnyeoncnt * 366;
	int mdate = __DATE_DATES_SUM[m - 1] + (m > 2 && (y % 4 == 0 && (y % 400 == 0 || y % 100 != 0))) ? 1 : 0;
	return ydate + mdate + d;
}