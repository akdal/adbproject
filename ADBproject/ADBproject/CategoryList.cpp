#include"CategoryList.h"
using namespace std;
#include<iostream>

void CategoryList::sort(){
	for (int i = 0; i < this->v.size(); i++){
		for (int j = i + 1; j < this->v.size(); j++){
			if (*(this->v[j]) == *(this->v[i])) continue;
			if ((*this->v[j]) < (*this->v[i])){
				swap(this->v[i], this->v[j]);
			}
		}
	}
}

double CategoryList::similarity(const CategoryList &in_a, const CategoryList &in_b){
	const CategoryList &a = in_a.v.size() > in_b.v.size() ? in_b : in_a;
	const CategoryList &b = in_a.v.size() <= in_b.v.size() ? in_b : in_a;

	const int acnt = a.v.size();
	const int bcnt = b.v.size();
	if(acnt == 0 || bcnt == 0)
		return 0;

	int *indices = new int[acnt];
	double *scores = new double[acnt];

	indices[0] = 0;
	scores[0] = Category::jaccardSimilarity(*a.v[0], *b.v[0]);
	for(int i = 1; i < bcnt; i++){
		double newscore = Category::jaccardSimilarity(*a.v[0], *b.v[i]);
		if(newscore > scores[0]){
			scores[0] = newscore;
			indices[0] = i;
		}else if(newscore < scores[0]){
			break;
		}
	}

	for(int i = 1; i < acnt; i++){
		indices[i] = indices[i - 1];
		scores[i] = Category::jaccardSimilarity(*a.v[i], *b.v[indices[i]]);

		while(indices[i] < bcnt){
			if(indices[i] == bcnt - 1) break;

			double newscore = Category::jaccardSimilarity(*a.v[i], *b.v[indices[i]]);
			if(newscore < scores[i])
				break;
			else{
				scores[i] = newscore;
				indices[i]++;
			}
		}
	}

	double totscore = 0;
	for(int i = 0; i < acnt; i++)
		totscore += scores[i];

	delete[] scores;
	delete[] indices;
	return totscore / acnt;
}
void CategoryList::merge(CategoryList *a, CategoryList *dst){
	std::vector<shared_ptr<Category> > orgv = dst->v;
	dst->v.clear();
	int dstsize = orgv.size();
	int srcsize = a->v.size();

	int aptr = 0, bptr = 0;
	while (aptr < srcsize && bptr < dstsize){
		if (*(a->v[aptr]) == *(orgv[bptr])){
			dst->v.push_back(orgv[bptr]);
			aptr++;
			bptr++;
		}
		else if (*(orgv[bptr]) < *(a->v[aptr])){
			dst->v.push_back(orgv[bptr]);
			bptr++;
		}
		else{
			dst->v.push_back(a->v[aptr]);
			aptr++;
		}
	}
	if (aptr < srcsize){
		dst->v.insert(dst->v.end(), a->v.begin() + aptr, a->v.end());
	}
	else if (bptr < dstsize){
		dst->v.insert(dst->v.end(), orgv.begin() + bptr, orgv.end());
	}

	/*
	for (int i = 1; i < dst->v.size(); i++){
		if (!(*(dst->v[i]) == *(dst->v[i - 1]) || *(dst->v[i - 1]) < *(dst->v[i]))){
			_ASSERTE(false);
		}
	}*/
}