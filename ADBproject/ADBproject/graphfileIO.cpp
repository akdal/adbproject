#include"graphfileIO.h"
#include"GraphNode.h"
#include"CategoryList.h"
#include"Category.h"
#include<memory>
using namespace std;

static void _writeNode(FILE *f, const shared_ptr<GraphNode> gn){
	fprintf(f, "nodeid %d\n", gn->nodeid);

	fprintf(f, "\ttimecolumn %d\n", gn->timecolumn);
	fprintf(f, "\tuserids %d", gn->userids.size());
	for(int i = 0; i < gn->userids.size(); i++)
		fprintf(f, " %d", gn->userids[i]);
	
	fprintf(f, "\n\tcategories %d\n", gn->categories.size());
	for(int i = 0; i < gn->categories.size(); i++){
		int sz = gn->categories[i]->cdata.size();
		fprintf(f, "\t%d", sz);
		for(int j =0;j < sz; j++)
			fprintf(f, " %d", gn->categories[i]->cdata[j]);
		fprintf(f, "\n");
	}
}
void writeGraph(FILE *f, const RawTimeGraph *tg){
	fprintf(f, "totedge %d totnode %d tottimecolumn %d\n", tg->totalEdgeCount(), tg->totalNodeCount(), tg->timecolumns.size());
	fprintf(f, "\n");

	for (int i = 0; i < tg->nodes.size(); i++)
		_writeNode(f, tg->nodes[i]);

	printf("graphfileIO : node writing end\n");
	for(auto itr = tg->adjlist.begin(); itr != tg->adjlist.end(); itr++){
		const int inode = (*itr).first;
		fprintf(f, "node %d nextnode %d\n", inode, (*itr).second);
		fprintf(f, "\n");
	}
}



static shared_ptr<GraphNode> _readNode(FILE *f, RawTimeGraph *tg){
	char forCheck[100];
	int nodeid;
	fscanf(f, "%s %d", forCheck, &nodeid);
	//_ASSERTE(strcmp(forCheck, "nodeid") == 0);

	shared_ptr<GraphNode> p(tg->addNode(nodeid));
	fscanf(f, " %*s %d", &(p->timecolumn));

	int usrcnt;
	fscanf(f, " %s %d", forCheck, &usrcnt);
	//_ASSERTE(strcmp(forCheck, "userids") == 0);
	for(int i = 0; i < usrcnt; i++){
		int n;
		fscanf(f, "%d", &n);
		p->userids.push_back(n);
	}

	int categorycnt;
	fscanf(f, " %s %d", forCheck, &categorycnt);
	//_ASSERTE(strcmp(forCheck, "categories") == 0);
	for(int i = 0; i < categorycnt; i++){
		int clen;
		fscanf(f, "%d", &clen);
		std::shared_ptr<Category> cate(new Category());
		while(clen--){
			int nn;
			fscanf(f, "%d", &nn);
			(*cate).cdata.push_back(nn);
		}
		p->categories.push_back(cate);
	}
	return p;
}

RawTimeGraph *readGraph(FILE *f){
	int edgcnt_for_verify, nodecnt, timecolumncnt_for_verify;
	fscanf(f, " %*s %d %*s %d %*s %d", &edgcnt_for_verify, &nodecnt, &timecolumncnt_for_verify);
	char forCheck[100];

	RawTimeGraph *tg = new RawTimeGraph();
	for (int i = 0; i < nodecnt; i++){
		_readNode(f, tg);
		if(i % 1000 == 0)
			printf("%dth item read\n", i);
	}

	for(int i = 0; i < nodecnt; i++){
		int from, to;
		fscanf(f, " %*s %d %*s %d", &from, &to);
		tg->addEdge(tg->nodes[from], tg->nodes[to]);
	}
	if(tg->timecolumns.size() != timecolumncnt_for_verify){
		printf("Timecolumn count is different!!! %d : %d\n", tg->timecolumns.size() , timecolumncnt_for_verify);
	}

	return tg;
}