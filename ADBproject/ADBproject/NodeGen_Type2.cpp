#include"NodeGen_Type2.h"
using namespace std;

static shared_ptr<CategoryList> _getCategoryOf(const AmazonData *ad, shared_ptr<Review> rv){
	return ad->getItem(rv->itemId)->categories;
}

void NodeGen_Type2::genNext(shared_ptr<GraphNode> &gn){
	_ASSERTE(hasNext());

	shared_ptr<Review> rv = this->reviews->at(ptr);

	gn->addUserID(this->userid);
	gn->categories.v = (_getCategoryOf(this->amazonData, rv))->v;
	gn->timecolumn = (rv->dates - this->beginningdate) / this->dateRange;
	ptr++;
	int sz = this->reviews->size();
	while(ptr < sz){
		rv = this->reviews->at(ptr);
		
		int nexttimecolumn = (rv->dates - this->beginningdate) / this->dateRange;

		if(nexttimecolumn == gn->timecolumn){
			CategoryList::merge(&(*(_getCategoryOf(this->amazonData, rv))), &gn->categories);
			ptr++;
		}else{
			_ASSERTE(nexttimecolumn > gn->timecolumn);
			break;
		}
	}
}