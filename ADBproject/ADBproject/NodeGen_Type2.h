#ifndef _NODEGEN_TYPE2_H
#define _NODEGEN_TYPE2_H

#include"NodeGenerator.h"
#include"AmazonData.h"
#include<vector>
/**
Type 2 : 상대 시간.
시작 시간 - 현재 시간을 7로 나눈 값이 모두 같을 것이다.
*/
class NodeGen_Type2: public NodeGenerator{
public:
	const AmazonData *amazonData;
	const std::vector<std::shared_ptr<Review> > *reviews;
	int userid;
	int ptr;
	int dateRange; // 기본값 : 7일!
	int beginningdate;

	
	NodeGen_Type2(AmazonData *amazonData, int dateRange){
		this->amazonData = amazonData;
		reviews = NULL;
		userid = 0;
		this->dateRange = dateRange;
	}

	
	virtual void init(const std::vector<std::shared_ptr<Review> > &usr_reviews, int userid){
		this->reviews = &usr_reviews;
		this->userid = userid;
		this->ptr = 0;
		this->beginningdate = usr_reviews[0]->dates;
	}
	virtual bool hasNext(){
		return this->ptr < this->reviews->size();
	}
	virtual void genNext(std::shared_ptr<GraphNode> &p);
};

#endif