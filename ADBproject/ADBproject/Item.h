#ifndef _ITEM_H
#define _ITEM_H

#include<vector>
#include<string>
#include"Review.h"
#include"CategoryList.h"
#include"Category.h"
#include<memory>

class Item{
public:
	int itemId;
	int salesRank;
	std::string ASINid;
	std::string title;
	std::string group;
	std::vector<std::string> similarItemASINs;
	std::shared_ptr<CategoryList> categories;
	std::vector<std::shared_ptr<Review> > reviews;
};

#endif