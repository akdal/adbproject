#include"ChronicleData.h"
using namespace std;

static bool _reviewcomp(const shared_ptr<Review> &r1, const shared_ptr<Review> &r2){
	return r1->dates < r2->dates;
}
void ChronicleData::sortReviews(){
	for(int i = 0; i < this->usercnt; i++)
		std::sort(reviewsByTime[i].begin(), reviewsByTime[i].end(), _reviewcomp);
}