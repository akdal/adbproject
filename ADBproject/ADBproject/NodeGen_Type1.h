#pragma once

#include"NodeGenerator.h"
#include"AmazonData.h"

/**
Type 1 : 절대 시간.
return하는 node안에 들어갈 review의 timestamp 값들은 7로 나눈 값이 항상 같다.
*/
class NodeGen_Type1 : public NodeGenerator{
public:
	const AmazonData *amazonData;
	const std::vector<std::shared_ptr<Review> > *reviews;
	int userid;
	int ptr;
	int dateRange; // 기본값 : 7일!

	NodeGen_Type1(AmazonData *amazonData, int dateRange){
		this->amazonData = amazonData;
		reviews = NULL;
		userid = 0;
		this->dateRange = dateRange;
	}

	virtual void init(const std::vector<std::shared_ptr<Review> > &usr_reviews, int userid){
		this->reviews = &usr_reviews;
		this->userid = userid;
		this->ptr = 0;
	}
	virtual bool hasNext(){
		return this->ptr < this->reviews->size();
	}
	virtual void genNext(std::shared_ptr<GraphNode> &p);
};
