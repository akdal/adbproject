#include"amazonDataGenerator.h"

#include<stdio.h>
#include<string>
#include<crtdbg.h>
#include<cstring>
#include"AmazonData.h"
#include"Item.h"
#include"Review.h"
#include"ChronicleData.h"
#include"CategoryList.h"
#include"RawTimeGraph.h"
#include"util.h"
using namespace std;

int __lineNumber = 0;
static void eraseEmptyPrefix(char *chrs){
	int ptr = 0;
	while(isspace(chrs[ptr])) ptr++;
	int ptr2 = 0;
	while(chrs[ptr]){
		chrs[ptr2] = chrs[ptr];
		ptr++;
		ptr2++;
	}
	chrs[ptr2] = 0;
}
static char *tokenize_next(char *ptr){
	while(!isspace(*ptr) && *ptr != 0)
		ptr++;
	return ptr;
}
static bool getNextLine(FILE *f, char *chrs){
	if(feof(f))
		return false;
	while(!feof(f)){
		fgets(chrs, 1000, f);
		if(chrs[0] == '#' || chrs[0] == '\n' || chrs[0] == '\r')
			continue;
		break;
	}
	__lineNumber++;
	eraseEmptyPrefix(chrs);
	return true;
}
void assertMatch(char *orgmst, const char *constpart){
	if(true) return;
	int l = strlen(constpart);
	for(int i = 0; i < l; i++){
		_ASSERTE(orgmst[i] == constpart[i]);
	}
}
static shared_ptr<Item> readItem(FILE *f, bool readsSimilarItem, bool readsCategory){
	char msg[10000];
	
	int itemid;
	char itemASIN[100];
	char title[1000];
	char group[1000];
	int rank;
	int similarcnt;
	int catecnt;
	int reviewcnt, reviewcnt_downloaded; // 두 개 차이가 뭐지?-_-;

	getNextLine(f, msg);
	assertMatch(msg, "Id:");
	sscanf(msg, "%*s %d", &itemid);
	getNextLine(f, msg);
	assertMatch(msg, "ASIN:");
	sscanf(msg, "%*s %s", &itemASIN);

	getNextLine(f, msg);
	if(strncmp(msg, "discontinued product", 20) == 0)
		return NULL;

	shared_ptr<Item> itm(new Item());
	itm->itemId = itemid;
	itm->ASINid = string(itemASIN);

	assertMatch(msg, "title:");
	itm->title = string(msg + 6);

	getNextLine(f, msg);
	assertMatch(msg, "group:");
	sscanf(msg, "%*s %s", group);
	itm->group = string(group);

	getNextLine(f, msg);
	assertMatch(msg, "salesrank");
	sscanf(msg, "%*s %d", &rank);
	itm->salesRank = rank;

	getNextLine(f, msg);
	assertMatch(msg, "similar:");
	sscanf(msg, "%*s %d", &similarcnt);
	if(readsSimilarItem)
		itm->similarItemASINs.reserve(similarcnt);
	{
		char *ptr = tokenize_next(msg); // just after "smilar"
		eraseEmptyPrefix(ptr);
		ptr = tokenize_next(ptr); // just after similarcnt
		eraseEmptyPrefix(ptr);
		for(int i = 0; i < similarcnt; i++){
			char *nptr = tokenize_next(ptr);
			if(readsSimilarItem)
				itm->similarItemASINs.push_back(string(ptr, nptr));

			ptr = nptr;
			eraseEmptyPrefix(ptr);
		}
	}
	getNextLine(f, msg);
	assertMatch(msg, "categories:");
	sscanf(msg, "%*s %d", &catecnt);
	if(readsCategory){
		itm->categories = shared_ptr<CategoryList>(new CategoryList());
		itm->categories->v.reserve(catecnt);
	}
	{
		for(int i =0 ; i < catecnt; i++){
			getNextLine(f, msg);
			if(readsCategory){
				itm->categories->v.push_back(Category::parseCategory(msg));
			}
		}
		if (readsCategory)
			itm->categories->sort();
	}
	getNextLine(f, msg);
	assertMatch(msg, "reviews:");
	sscanf(msg, "%*s %*s %d %*s %d %*s %*s %*d", &reviewcnt, &reviewcnt_downloaded);
	itm->reviews.reserve(reviewcnt_downloaded);
	//_ASSERTE(reviewcnt == reviewcnt_downloaded);
	{
		for(int i = 0; i < reviewcnt_downloaded; i++){
			shared_ptr<Review> rev(new Review());
			char date[20];
			char tmp[100];
			int customerid;
			int rating;
			int votes;
			int helpfulness;

			getNextLine(f, msg);
			sscanf(msg, "%s	%s %d %*s %d %*s %d %*s %d", date, tmp, &customerid, &rating, &votes, &helpfulness);
			assertMatch(tmp, "cutomer:"); // cutomer이 맞음 ㅇㅇ customer ㄴㄴ

			rev->customer = (customerid);
			rev->rating = (rating);
			rev->vote = (votes);
			rev->helpfulness = (helpfulness);
			rev->dates = (date_strToInt(date));
			rev->itemId = itm->itemId;
			itm->reviews.push_back(rev);
		}
	}

	return itm;
}


AmazonData *readFile(FILE *f, bool readsSimilarItem, bool readsCategory, int maxItemCnt){
	__lineNumber = 0;

	int totalItemCnt;

	char msg[1000];
	getNextLine(f, msg);
	assertMatch(msg, "Total items: ");
	sscanf(msg, "%*s %*s %d", &totalItemCnt);
	printf("total Item count : %d\n", totalItemCnt);

	AmazonData *amazonData=  new AmazonData(totalItemCnt);
	int removeditmcnt = 0;
	int totalReviewCnt = 0;
	int catezerocnt = 0;
	for(int i = 0; i < totalItemCnt; i++){
		if(maxItemCnt != -1 && maxItemCnt <= i)
			break;
		shared_ptr<Item> itm = readItem(f, readsSimilarItem, readsCategory);
		if(itm){
			amazonData->registerItem(itm);
			totalReviewCnt += itm->reviews.size();
			if(itm->categories->size() == 0)
				catezerocnt++;
		}else
			removeditmcnt++;
		if(i % 1000 == 0)
			printf("%d\n", i);
	}
	printf("Reading end! removed item cnt : %d\n", removeditmcnt);
	printf("\ttotal reviewcnt : %d\n", totalReviewCnt);
	printf("\titems that have no category : %d\n", catezerocnt);
	printf("processing end!\n");
	return amazonData;
}


ChronicleData *generateChronicleData(AmazonData *ad){
	int maxusrcnt = -1;
	for(int i = 0; i < ad->getItemCount(); i++){
		if(ad->items[i] == NULL) continue;
		for(int j = 0; j < ad->items[i]->reviews.size(); j++){
			maxusrcnt = max(maxusrcnt, ad->items[i]->reviews[j]->customer);
		}
	}
	maxusrcnt++;
	printf("generateChronicleData : maxusrcnt: %d\n", maxusrcnt);
	ChronicleData *cd = new ChronicleData(maxusrcnt);
	
	for(int i = 0; i < ad->getItemCount(); i++){
		if(ad->items[i] == NULL) continue;
		for(int j = 0; j < ad->items[i]->reviews.size(); j++){
			int cid =  ad->items[i]->reviews[j]->customer;
			cd->addReview(cid, ad->items[i]->reviews[j]);
		}
	}
	cd->sortReviews();
	return cd;
}


RawTimeGraph *generateRawTimeGraph(ChronicleData *cdata, vector<int> &targetUserIds, NodeGenerator *ng){
	RawTimeGraph *tg = new RawTimeGraph();

	int maxCategoryCnt = 0;
	for(int i = 0; i < targetUserIds.size(); i++){
		int usrid = targetUserIds[i];
		ng->init(cdata->reviewsByTime[usrid], usrid);
		shared_ptr<GraphNode> befp(NULL);
		while(ng->hasNext()){
			shared_ptr<GraphNode> p = tg->addNode();
			ng->genNext(p);
			if (p->categories.size() > maxCategoryCnt)
				maxCategoryCnt = p->categories.size();
			
			if(befp != NULL){
				tg->addEdge(befp, p);
			}
			befp = p;
		}
		if(i % 500 == 0)
			printf("generateTimeGraph : generated : %dth ; usrid : %d\n", i, usrid);
	}
	printf("DONE! max(node[i].category.size()) = %d , timecolumn cnt : %d\n", maxCategoryCnt, tg->timecolumns.size());
	return tg;
}