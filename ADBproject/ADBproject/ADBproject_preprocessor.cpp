/*
http://snap.stanford.edu/data/amazon-meta.html
Reads raw file(amazon-meta.txt), and make preprocssed file amazon-meta-preprocessed.txt
All customers are replaced from hex code into a number
*/

#include<stdio.h>
#include"Trie.h"

Trie<int> customerTrie;


int inlinecnt = 0;
int outlinecnt = 0;
void getline(FILE *f, char *c){
	fgets(c, 1000, f);
	inlinecnt++;
}
void putline(FILE *f, char *c){
	fputs(c, f);
	outlinecnt++;
	_ASSERTE(outlinecnt == inlinecnt);
}

static int registerCustomer(const char *c){
	int *p = customerTrie.get(c);
	if(p != NULL)
		return *p;
	int sz = customerTrie.size();
	customerTrie.put(c, sz);
	return sz;
}

int main(int argc, char **argv)
{
	if(argc< 3){
		printf("ADBproject_preprocessor.exe <amazon-meta.txt> <amazon-meta-preprocessed.txt>\n");
		return 0;
	}

	FILE *fin = fopen(argv[1], "r");
	if(!fin){
		printf("%s file doesn't exist\n", argv[1]);
		return 0;
	}
	FILE *fout = fopen(argv[2], "w");

	printf("preprocessor start!\n");
	char buf[1000];
	int id = 0;
	char usrname[100];
	char firstWord[100];		

	while(!feof(fin)){
		getline(fin, buf);
		if(buf[0] != '\r' || buf[0] != '\n'){
			sscanf(buf, "%s", firstWord);
			
			if(strncmp(firstWord, "reviews:", 8) == 0){
				int revcnt = 0;
				putline(fout, buf);
				sscanf(buf, "%*s %*s %*s %*s %d", &revcnt);

				for(int i = 0; i < revcnt; i++){
					getline(fin, buf);
					sscanf(buf, "%*s %*s %s", usrname);
					int id = registerCustomer(usrname);

					for(int j = 0; j < 30; j++){
						if(buf[j] == ':'){
							j++;
							while(isspace(buf[j])) j++;
							int start=  j;
							while(!isspace(buf[j])){
								buf[j] = ' ';
								j++;
							}
							int cnt = sprintf(buf + start, "%d", id);
							buf[start + cnt] = ' ';
						}
					}
					putline(fout, buf);
				}
			}else{
				putline(fout, buf);
			}
		}else
			putline(fout, buf);
		if(inlinecnt % 2000 == 0)
			printf("(about) %d\n", inlinecnt);
	}
	fclose(fin);
	fclose(fout);
	printf("END! total usercount :%d\n", customerTrie.size());
	printf("\n");

	return 0;
}