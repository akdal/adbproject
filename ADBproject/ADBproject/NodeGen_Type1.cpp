#include"NodeGen_Type1.h"
#include<crtdbg.h>
#include<stdio.h>
using namespace std;

static std::shared_ptr<CategoryList> _getCategoryOf(const AmazonData *ad, std::shared_ptr<Review> rv){
	auto aa = ad->getItem(rv->itemId);
	_ASSERTE(aa !=NULL);
	return aa->categories;
}

void NodeGen_Type1::genNext(std::shared_ptr<GraphNode> &gn){
	_ASSERTE(hasNext());

	std::shared_ptr<Review> rv = this->reviews->at(ptr);

	gn->addUserID(this->userid);
	gn->categories.v = (_getCategoryOf(this->amazonData, rv))->v;
	gn->timecolumn = rv->dates / this->dateRange;
	ptr++;
	int sz = this->reviews->size();
	while(ptr < sz){
		rv = this->reviews->at(ptr);
		
		if(rv->dates / this->dateRange == gn->timecolumn){
			CategoryList::merge(&(*(_getCategoryOf(this->amazonData, rv))), &gn->categories);
			ptr++;
		}else{
			_ASSERTE(rv->dates / this->dateRange > gn->timecolumn);
			break;
		}
	}
}