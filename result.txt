중간과정에서 0.2 보다 낮아지면 걍 break 했더니 결과가 그닥 좋지 않음. 그러니까 ret_value 자체가 너무 낮게 나옴.

~~~~

SIMILARITY THRESHOLD 0.5, FACTOR 0.5
그렇지 않고 그냥 끝까지 가니까 결과 엄청 많이 나옴 -> 중간에 크게 달라지는 경우가 매우 많음.

start calculation from 40 to 50 ...
0 : ret_value : 4, group # : 3040(4), same group # : 248, real : 0
400 : ret_value : 6015, group # : 1633(1411), same group # : 75107, real : 940
800 : ret_value : 9887, group # : 1330(1714), same group # : 128789, real : 1777
1200 : ret_value : 13469, group # : 1105(1939), same group # : 174361, real : 2687
1600 : ret_value : 16664, group # : 985(2059), same group # : 212322, real : 3445
2000 : ret_value : 18900, group # : 927(2117), same group # : 239533, real : 4179
2400 : ret_value : 20288, group # : 885(2159), same group # : 257779, real : 4850
2800 : ret_value : 21092, group # : 869(2175), same group # : 268569, real : 5329
end : ret_value : 21232, group # : 865(2179), same group # : 270091, real : 5396
from 40 to 50 : 21232 !
calculation end!
start calculation from 40 to 60 ...
0 : ret_value : 7, group # : 3037(7), same group # : 248, real : 7
400 : ret_value : 2232, group # : 1935(1109), same group # : 75107, real : 1505
800 : ret_value : 3944, group # : 1545(1499), same group # : 128789, real : 2766
1200 : ret_value : 5613, group # : 1285(1759), same group # : 174361, real : 4127
1600 : ret_value : 7083, group # : 1153(1891), same group # : 212322, real : 5380
2000 : ret_value : 8287, group # : 1074(1970), same group # : 239533, real : 6515
2400 : ret_value : 9320, group # : 1009(2035), same group # : 257779, real : 7527
2800 : ret_value : 10022, group # : 986(2058), same group # : 268569, real : 8221
end : ret_value : 10124, group # : 982(2062), same group # : 270091, real : 8323
from 40 to 60 : 10124 !
calculation end!
start calculation from 40 to 70 ...
0 : ret_value : 8, group # : 3036(8), same group # : 248, real : 8
400 : ret_value : 1885, group # : 1935(1109), same group # : 75107, real : 1731
800 : ret_value : 3399, group # : 1552(1492), same group # : 128789, real : 3159
1200 : ret_value : 5000, group # : 1260(1784), same group # : 174361, real : 4723
1600 : ret_value : 6449, group # : 1101(1943), same group # : 212322, real : 6146
2000 : ret_value : 7684, group # : 1016(2028), same group # : 239533, real : 7378
2400 : ret_value : 8759, group # : 942(2102), same group # : 257779, real : 8449
2800 : ret_value : 9465, group # : 921(2123), same group # : 268569, real : 9154
end : ret_value : 9574, group # : 916(2128), same group # : 270091, real : 9263
from 40 to 70 : 9574 !
calculation end!
start calculation from 40 to 80 ...
0 : ret_value : 8, group # : 3036(8), same group # : 248, real : 8
400 : ret_value : 1890, group # : 1901(1143), same group # : 75107, real : 1857
800 : ret_value : 3411, group # : 1500(1544), same group # : 128789, real : 3359
1200 : ret_value : 5047, group # : 1194(1850), same group # : 174361, real : 4984
1600 : ret_value : 6510, group # : 1033(2011), same group # : 212322, real : 6438
2000 : ret_value : 7764, group # : 946(2098), same group # : 239533, real : 7690
2400 : ret_value : 8851, group # : 870(2174), same group # : 257779, real : 8775
2800 : ret_value : 9558, group # : 849(2195), same group # : 268569, real : 9482
end : ret_value : 9667, group # : 844(2200), same group # : 270091, real : 9591
from 40 to 80 : 9667 !
calculation end!
start calculation from 40 to 90 ...
0 : ret_value : 8, group # : 3036(8), same group # : 248, real : 8
400 : ret_value : 1915, group # : 1874(1170), same group # : 75107, real : 1907
800 : ret_value : 3446, group # : 1474(1570), same group # : 128789, real : 3434
1200 : ret_value : 5101, group # : 1165(1879), same group # : 174361, real : 5088
1600 : ret_value : 6563, group # : 1005(2039), same group # : 212322, real : 6548
2000 : ret_value : 7821, group # : 916(2128), same group # : 239533, real : 7806
2400 : ret_value : 8907, group # : 842(2202), same group # : 257779, real : 8892
2800 : ret_value : 9614, group # : 821(2223), same group # : 268569, real : 9599
end : ret_value : 9723, group # : 816(2228), same group # : 270091, real : 9708
from 40 to 90 : 9723 !
calculation end!

40 ~ 50 보다 40 ~ 60 오차 엄청큼 -> 40 ~ 50 이 되게 불안정함

but 그 후로는 9500 라인으로 안정

--> 그럭저럭 안정한 데다가 같은 그룹 사람들이 쭉 같이 가는 경향이 있다고 추측할 수 있음


~~~~~~~~~~~~~~~~~~~~


threshold 를 (to - from) * factor 로 두고, factor = 0.6 이라고 두었음. 공통인 애들이 (to - from) * factor 만큼 있어야 한다는 뜻. 

두 Node의 distance는 아직 0.5로 맞추어 놓았음

그리고 from 에서 to 까지 비슷하면 from 에서 110 번째 column 까지도 비슷할 것이다! 를 보이기 위해서 real 값을 출력하였음. real 값이 ret_value 값과 거의 비슷하면 성공. 그 결과로

start calculation from 40 to 50 ...
0 : ret_value : 0, group # : 3044(0), same group # : 248, real : 0
400 : ret_value : 2930, group # : 2059(985), same group # : 75107, real : 694
800 : ret_value : 4868, group # : 1762(1282), same group # : 128789, real : 1343
1200 : ret_value : 6694, group # : 1534(1510), same group # : 174361, real : 2042
1600 : ret_value : 8377, group # : 1425(1619), same group # : 212322, real : 2615
2000 : ret_value : 9563, group # : 1352(1692), same group # : 239533, real : 3233
2400 : ret_value : 10391, group # : 1311(1733), same group # : 257779, real : 3780
2800 : ret_value : 10899, group # : 1292(1752), same group # : 268569, real : 4173
end : ret_value : 10985, group # : 1288(1756), same group # : 270091, real : 4230
from 40 to 50 : 10985 !
calculation end!

start calculation from 40 to 60 ...
0 : ret_value : 3, group # : 3041(3), same group # : 248, real : 3
400 : ret_value : 1579, group # : 2166(878), same group # : 75107, real : 1315
800 : ret_value : 2850, group # : 1824(1220), same group # : 128789, real : 2428
1200 : ret_value : 4151, group # : 1590(1454), same group # : 174361, real : 3636
1600 : ret_value : 5300, group # : 1455(1589), same group # : 212322, real : 4706
2000 : ret_value : 6354, group # : 1376(1668), same group # : 239533, real : 5749
2400 : ret_value : 7276, group # : 1316(1728), same group # : 257779, real : 6665
2800 : ret_value : 7910, group # : 1290(1754), same group # : 268569, real : 7297
end : ret_value : 8006, group # : 1286(1758), same group # : 270091, real : 7393

from 40 to 60 : 8006 !
calculation end!

start calculation from 40 to 70 ...
0 : ret_value : 8, group # : 3036(8), same group # : 248, real : 8
400 : ret_value : 1644, group # : 2075(969), same group # : 75107, real : 1598
800 : ret_value : 3008, group # : 1713(1331), same group # : 128789, real : 2938
1200 : ret_value : 4475, group # : 1438(1606), same group # : 174361, real : 4394
1600 : ret_value : 5821, group # : 1293(1751), same group # : 212322, real : 5728
2000 : ret_value : 7020, group # : 1209(1835), same group # : 239533, real : 6925
2400 : ret_value : 8076, group # : 1138(1906), same group # : 257779, real : 7981
2800 : ret_value : 8780, group # : 1116(1928), same group # : 268569, real : 8685
end : ret_value : 8889, group # : 1111(1933), same group # : 270091, real : 8794

from 40 to 70 : 8889 !
calculation end!
start calculation from 40 to 80 ...
0 : ret_value : 8, group # : 3036(8), same group # : 248, real : 8
400 : ret_value : 1775, group # : 1979(1065), same group # : 75107, real : 1766
800 : ret_value : 3232, group # : 1595(1449), same group # : 128789, real : 3215
1200 : ret_value : 4817, group # : 1313(1731), same group # : 174361, real : 4800
1600 : ret_value : 6261, group # : 1157(1887), same group # : 212322, real : 6242
2000 : ret_value : 7506, group # : 1071(1973), same group # : 239533, real : 7486
2400 : ret_value : 8581, group # : 1000(2044), same group # : 257779, real : 8561
2800 : ret_value : 9288, group # : 979(2065), same group # : 268569, real : 9268
end : ret_value : 9397, group # : 974(2070), same group # : 270091, real : 9377
from 40 to 80 : 9397 !
calculation end!
start calculation from 40 to 90 ...
0 : ret_value : 8, group # : 3036(8), same group # : 248, real : 8
400 : ret_value : 1857, group # : 1923(1121), same group # : 75107, real : 1857
800 : ret_value : 3359, group # : 1533(1511), same group # : 128789, real : 3359
1200 : ret_value : 4984, group # : 1234(1810), same group # : 174361, real : 4984
1600 : ret_value : 6438, group # : 1075(1969), same group # : 212322, real : 6438
2000 : ret_value : 7690, group # : 989(2055), same group # : 239533, real : 7690
2400 : ret_value : 8775, group # : 915(2129), same group # : 257779, real : 8775
2800 : ret_value : 9482, group # : 894(2150), same group # : 268569, real : 9482
end : ret_value : 9591, group # : 889(2155), same group # : 270091, real : 9591
from 40 to 90 : 9591 !
calculation end!

40 ~ 50은 오차가 큰 것이 증명되었고 40 ~ 60부터 ret_value와 real 차이가 그리 크지 않음. (8000개 중에서 7400개가 수렴)

40 ~ 70 은 거의 똑같은데 이건 굉장히 놀라운 결과임. 왜냐하면 30 column 만 보고 수렴하는 것을 예상하였는데

110까지 남은 column은 40 column 임에도 불구하고 비슷하게 흘러간다는 결과를 알 수 있음. 기분 개좋음.

40 ~ 80 까지는 이제 거의 똑같은데, 40 -> 80 까지 40 column 지나가고 80 ~ 110 까지 30 column 지나감.

즉, 40번 진행하게 되면 그 후 30번은 거의 정확하게 예측할 수 있다는 것.



factor는 두 Node가 column을 전진하면서 공통된 갯수에 해당하는 것이며

0.5라는 threshold 는 두 Node의 similarity 가 이 이상이면 비슷하다고 판단하는 기준임.

사실 0.5인 애들을 Grouping 하는건 정당하다고 생각하는데,

A 가 B와 0.5 만큼 비슷하고, A가 C와 0.5 만큼 비슷하다고 하자. 즉 B ~ A ~ C 의 그림인데

여기서 최악의 상황에서는 B와 C가 아예 다를 수 있음. 그럴 수 있는데 어떤 D가 A와 또 비슷하다고 생각을 하면

D는 B 혹은 C와 적어도 0.5는 비슷해야 한다는 것을 예상할 수 있음. 그러니까 A와 비슷한 아이들만 쭉 모아놓아도

걔들끼리도 서로 비슷할 것이라는 것을 예상할 수 있음. --> similarity 를 0.5로 정한 것이 그럴싸한 이유


40 ~ 70 은 거의 똑같은데 그 결과가 40 ~ 110 까지의 결과와 매우 비슷하다는 것이 굉장히 흥미로운 점이고

우리가 세웠던 가설을 거의 성공적으로 증명한 듯 함.

~~

이제 Similarity Threshold를 0.6으로 올려보았음. 즉 더 많이 가까워야 한다는 뜻. 예상되는 결과로써 더 많이 같다면 더 정확하게 미래를 예측할 수 있다고 생각함. 그러면 real과 ret_value 사이의 차이가 더욱 감소되어야 함.

start calculation from 40 to 50 ...
0 : ret_value : 0, group # : 3044(0), same group # : 97, real : 0
400 : ret_value : 849, group # : 2625(419), same group # : 36709, real : 323
800 : ret_value : 1572, group # : 2405(639), same group # : 65916, real : 673
1200 : ret_value : 2130, group # : 2235(809), same group # : 88171, real : 999
1600 : ret_value : 2651, group # : 2152(892), same group # : 106712, real : 1246
2000 : ret_value : 3098, group # : 2090(954), same group # : 120437, real : 1577
2400 : ret_value : 3470, group # : 2038(1006), same group # : 129436, real : 1904
2800 : ret_value : 3698, group # : 2017(1027), same group # : 134877, real : 2111
end : ret_value : 3731, group # : 2013(1031), same group # : 135679, real : 2140

from 40 to 50 : 3731 !
calculation end!
start calculation from 40 to 60 ...
0 : ret_value : 0, group # : 3044(0), same group # : 97, real : 0
400 : ret_value : 691, group # : 2567(477), same group # : 36709, real : 640
800 : ret_value : 1364, group # : 2298(746), same group # : 65916, real : 1261
1200 : ret_value : 2005, group # : 2094(950), same group # : 88171, real : 1888
1600 : ret_value : 2516, group # : 1986(1058), same group # : 106712, real : 2381
2000 : ret_value : 3085, group # : 1901(1143), same group # : 120437, real : 2948
2400 : ret_value : 3592, group # : 1829(1215), same group # : 129436, real : 3455
2800 : ret_value : 3931, group # : 1800(1244), same group # : 134877, real : 3794
end : ret_value : 3980, group # : 1794(1250), same group # : 135679, real : 3843

from 40 to 60 : 3980 !
calculation end!
start calculation from 40 to 70 ...
0 : ret_value : 2, group # : 3042(2), same group # : 97, real : 2
400 : ret_value : 780, group # : 2500(544), same group # : 36709, real : 777
800 : ret_value : 1544, group # : 2208(836), same group # : 65916, real : 1535
1200 : ret_value : 2269, group # : 1957(1087), same group # : 88171, real : 2260
1600 : ret_value : 2908, group # : 1814(1230), same group # : 106712, real : 2894
2000 : ret_value : 3563, group # : 1704(1340), same group # : 120437, real : 3548
2400 : ret_value : 4135, group # : 1610(1434), same group # : 129436, real : 4120
2800 : ret_value : 4506, group # : 1583(1461), same group # : 134877, real : 4491
end : ret_value : 4560, group # : 1577(1467), same group # : 135679, real : 4545

from 40 to 70 : 4560 !
calculation end!
start calculation from 40 to 80 ...
0 : ret_value : 2, group # : 3042(2), same group # : 97, real : 2
400 : ret_value : 864, group # : 2434(610), same group # : 36709, real : 864
800 : ret_value : 1690, group # : 2117(927), same group # : 65916, real : 1688
1200 : ret_value : 2503, group # : 1839(1205), same group # : 88171, real : 2501
1600 : ret_value : 3198, group # : 1675(1369), same group # : 106712, real : 3195
2000 : ret_value : 3873, group # : 1559(1485), same group # : 120437, real : 3870
2400 : ret_value : 4455, group # : 1467(1577), same group # : 129436, real : 4452
2800 : ret_value : 4827, group # : 1440(1604), same group # : 134877, real : 4824
end : ret_value : 4881, group # : 1434(1610), same group # : 135679, real : 4878

from 40 to 80 : 4881 !
calculation end!
start calculation from 40 to 90 ...
0 : ret_value : 2, group # : 3042(2), same group # : 97, real : 2
400 : ret_value : 913, group # : 2395(649), same group # : 36709, real : 913
800 : ret_value : 1763, group # : 2071(973), same group # : 65916, real : 1763
1200 : ret_value : 2597, group # : 1782(1262), same group # : 88171, real : 2597
1600 : ret_value : 3297, group # : 1617(1427), same group # : 106712, real : 3297
2000 : ret_value : 3980, group # : 1494(1550), same group # : 120437, real : 3980
2400 : ret_value : 4565, group # : 1400(1644), same group # : 129436, real : 4565
2800 : ret_value : 4937, group # : 1373(1671), same group # : 134877, real : 4937
end : ret_value : 4991, group # : 1367(1677), same group # : 135679, real : 4991

from 40 to 90 : 4991 !
calculation end!

이전 실험에서 40 ~ 60 의 ret_value 와 real 값의 차이가 700 정도 났던 반면 지금은 140? 정도밖에 나지않음

40 ~ 70 의 ret_value는 이전 실험에서 약 100정도 차이가 났는데 이번에는 15밖에 차이가 나지 않음. 

확실히 비슷한 아이를 따라갈 수록 그 예상이 더욱 더 정확하다는 것을 알 수 있음.

하지만 예측할 수 있는 갯수가 너무 적음. ( same group # 과 ret_value 의 차이 )




~~~

지금 실험에서는 Similarity Threshold 를 0.6 으로 두었는데, 사실 이거보다는 0.5가 더 무난하다고 생각됨.

하지만 0.5라도 ret_value와 same group # 의 차이는 좀 많이 나는데, 이걸 생각해보면은

처음에 같다고 해서 ( 그 갯수가 same group # ) 그 후의 trajectory 가 같다고는 생각할 수 없다는 것.

사실 어느정도 맞는 말인게 처음에는 어쩌다가 비슷한 그룹에 낄 수 있기 때문.

그것보다 더 그럴싸 하고 괜찮은 것은 "처음에 Tracjectory 가 어느정도 비슷하다면" "그 후의 Trajectory 역시 비슷할 것이다."

가 더 Reasonable 한 것 같은데 우리의 결과가 그럴싸하게 나왔음. Time column 이 더 있었다면 더 괜찮은 실험이 되었을 것 같음.

~~~
